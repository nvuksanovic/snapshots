package app.snapshot_bitcake;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import app.AppConfig;
import servent.handler.TransactionHandler;
import servent.message.TransactionMessage;

/**
 * Main snapshot collector class. Has support for Naive, Chandy-Lamport
 * and Lai-Yang snapshot algorithms.
 * 
 * @author bmilojkovic
 *
 */
public class SnapshotCollectorWorker implements SnapshotCollector {

	private volatile boolean working = true;
	
	private AtomicBoolean collecting = new AtomicBoolean(false);
	
	private Map<Integer, LYSnapshotResult> collectedLYValues = new ConcurrentHashMap<>();
	
	private BitcakeManager bitcakeManager;
	private int snapshotNumber = 0;

	public SnapshotCollectorWorker() {
		bitcakeManager = new LaiYangBitcakeManager();
	}
	
	@Override
	public BitcakeManager getBitcakeManager() {
		return bitcakeManager;
	}
	
	@Override
	public void run() {
		while(working) {
			
			/*
			 * Not collecting yet - just sleep until we start actual work, or finish
			 */
			while (collecting.get() == false) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (working == false) {
					return;
				}
			}
			
			/*
			 * Collecting is done in three stages:
			 * 1. Send messages asking for values
			 * 2. Wait for all the responses
			 * 3. Print result
			 */
			
			//1 send asks
			AppConfig.myServentInfo.setMaster(AppConfig.myServentInfo.getId());
			AppConfig.myServentInfo.setInitiator(true);
			((LaiYangBitcakeManager)bitcakeManager).markerEvent(AppConfig.myServentInfo.getId(), this, AppConfig.myServentInfo.getId());
			
			
			//2 wait for responses or finish
			boolean waiting = true;
			while (waiting) {
				collectedLYValues.putAll(AppConfig.myServentInfo.getSnapshotResult());

				if (AppConfig.myServentInfo.isFinished()) {
					snapshotNumber++;
					waiting = false;
				}
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (working == false) {
					return;
				}
			}
			
			
			//print
			int sum;
			sum = 0;
			for (Entry<Integer, LYSnapshotResult> nodeResult : collectedLYValues.entrySet()) {
				sum += nodeResult.getValue().getRecordedAmount();
				AppConfig.timestampedStandardPrint(
						"Recorded bitcake amount for " + nodeResult.getKey() + " = " + nodeResult.getValue().getRecordedAmount());
			}
		
			System.out.println("idBorderSet: " + AppConfig.myServentInfo.getIdBorderSet());
			int i, j;
			
			for(Entry<Integer, LYSnapshotResult> entryi: collectedLYValues.entrySet()) {
				for (Entry<Integer, LYSnapshotResult> entryj: collectedLYValues.entrySet()) {
					i = entryi.getKey();
					j = entryj.getKey();
					if (i != j) {
						if (AppConfig.getInfoById(i).getNeighbors().contains(j) &&
							AppConfig.getInfoById(j).getNeighbors().contains(i)) {
							int ijAmount = filterGive(collectedLYValues, i, j);
							int jiAmount = filterGet(collectedLYValues, j, i);
							//int ijAmount = collectedLYValues.get(i).getGiveHistory().get(j);
							//int jiAmount = collectedLYValues.get(j).getGetHistory().get(i);
							//System.out.println("Cvor " + i + " zeli da posalje cvoru " + j + ": "+ collectedLYValues.get(i).getGiveHistory().get(j));
							//System.out.println("Cvor " + j + " misli da je dobio od cvora " + i + ": "+ collectedLYValues.get(j).getGetHistory().get(i));
							if (ijAmount != jiAmount) {
								System.out.println("Amounts: " + ijAmount + " " + jiAmount);
								String outputString = String.format(
										"Unreceived bitcake amount: %d from servent %d to servent %d",
										ijAmount - jiAmount, i, j);
								AppConfig.timestampedStandardPrint(outputString);
								sum += ijAmount - jiAmount;
							}
						}
					}
				}
			}
			
			AppConfig.timestampedStandardPrint("System bitcake count: " + sum);
			
			
			/*
			if(collectedLYValues.containsKey(2)) {
				System.out.println("GIVE: " + filter(collectedLYValues, 2, 3));
				System.out.println("GET: " + filter(collectedLYValues, 2, 3));
			}
			else {
				System.out.println("GIVE: " + filter(collectedLYValues, 3, 2));
				System.out.println("GET: " + filter(collectedLYValues, 3, 2));
			}*/
			/*
			if(collectedLYValues.containsKey(2)) {
				System.out.println("GET " + collectedLYValues.get(2).getGetHistory());
				System.out.println("GIVE " + collectedLYValues.get(2).getGetHistory());
			}else {
				System.out.println("GET " + collectedLYValues.get(3).getGetHistory());
				System.out.println("GIVE " + collectedLYValues.get(3).getGetHistory());
			}*/
			
			collectedLYValues.clear(); //reset for next invocation
			collecting.set(false);
			
		}

	}
	
	@Override
	public void addLYSnapshotInfo(int id, LYSnapshotResult lySnapshotResult) {
		collectedLYValues.put(id, lySnapshotResult);
	}
	
	@Override
	public void startCollecting() {
		boolean oldValue = this.collecting.getAndSet(true);
		
		if (oldValue == true) {
			AppConfig.timestampedErrorPrint("Tried to start collecting before finished with previous.");
		}
	}
	
	public int filterGive(Map<Integer, LYSnapshotResult> collectedLYValues, int i, int j) {
		int sum = 0;
		for(TransactionMessage tm: collectedLYValues.get(i).getGiveHistory()) {
	
			if(tm.getReceiverInfo().getId() == j)
				//System.out.println("da");
			
			if(tm.getOriginalSenderInfo().getId() == i)
				//System.out.println("da2");
			
			//System.out.println("Sender: " + tm.getOriginalSenderInfo().getId() + "Rec: " + tm.getReceiverInfo().getId());
			//System.out.println("i " + i + " j " + j);
			//System.out.println("ID receivera: " + tm.getReceiverInfo().getId() + " cvor j: " + j ); 
			//System.out.println("SN: " + tm.getSnapshotInfo().get(AppConfig.myServentInfo.getId()) + " " + (snapshotNumber));
			if(tm.getReceiverInfo().getId() == j && tm.getSnapshotInfo().get(AppConfig.myServentInfo.getId()) == (snapshotNumber-1)) {
				sum += Integer.parseInt(tm.getMessageText());
				//System.out.println("bla");
			}
		}
		
		//System.out.println("GIVE: " + i + " -> " + j + " " + sum);

		return sum;	
	}
	
	public int filterGet(Map<Integer, LYSnapshotResult> collectedLYValues, int i, int j) {
		int sum = 0;

		for(TransactionMessage tm: collectedLYValues.get(i).getGetHistory()) {
			//System.out.println("ID receivera: " + tm.getReceiverInfo().getId() + " cvor j: " + j + 
				//	" cvor i: " + i + " ID sendera: " + tm.getOriginalSenderInfo().getId());
			if(tm.getOriginalSenderInfo().getId() == j && tm.getSnapshotInfo().get(AppConfig.myServentInfo.getId()) == snapshotNumber-1) {
				sum += Integer.parseInt(tm.getMessageText());
				//System.out.println("Reciever: " + tm.getReceiverInfo().getId() + " sender " + j);

			}
		}
		
		//System.out.println("GET: " + i + " -> " + j + " " + sum);

		return sum;	
	}
	
	@Override
	public void stop() {
		working = false;
	}
	
	public int getId() {
		return AppConfig.myServentInfo.getId();
	}

}
