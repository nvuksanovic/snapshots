package app.snapshot_bitcake;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import app.AppConfig;
import app.ServentInfo;
import servent.message.Message;
import servent.message.TellParentInfoMessage;
import servent.message.TransactionMessage;
import servent.message.snapshot.LYMarkerMessage;
import servent.message.snapshot.LYTellMessage;
import servent.message.util.MessageUtil;

public class LaiYangBitcakeManager implements BitcakeManager {

	private final AtomicInteger currentAmount = new AtomicInteger(1000);
	
	public void takeSomeBitcakes(int amount) {
		currentAmount.getAndAdd(-amount);
	}
	
	public void addSomeBitcakes(int amount) {
		currentAmount.getAndAdd(amount);
	}
	
	public int getCurrentBitcakeAmount() {
		return currentAmount.get();
	}
	
	//svaki cvor ce da ima onoliko history mapa koliko ima inicijatora (AppConfig.getInitiatorsList().size())
	//znaci konkurentna mapa gde je key id inicijatora a value konkurentna mapa int int 
	//Map<Integer, ConcurrentHashMap<Integer, Integer>> give/getHistory = new ConcurrentHashMap<>();
	//mislim da je ok da se ovde kreira, jer ovde ulazi svaki cvor
	
	
	private Map<Integer, Integer> giveHistory = new ConcurrentHashMap<>();
	private Map<Integer, Integer> getHistory = new ConcurrentHashMap<>();
	
	private ArrayList<TransactionMessage> LiGetHistory = new ArrayList<TransactionMessage>();
	private ArrayList<TransactionMessage> LiGiveHistory = new ArrayList<TransactionMessage>();
	
	
	public LaiYangBitcakeManager() {

		for(Integer neighbor : AppConfig.myServentInfo.getNeighbors()) {
			giveHistory.put(neighbor, 0);
			getHistory.put(neighbor, 0);
		}
		
		
		for(int i = 0; i < AppConfig.getInitiatorsList().size(); i++) {
			AppConfig.myServentInfo.getSnapshotInfo().put(AppConfig.getInitiatorsList().get(i), 0);
		}
		
				
	}
	
	/*
	 * This value is protected by AppConfig.colorLock.
	 * Access it only if you have the blessing.
	 */
	public int recordedAmount = 0;
	
	public void snapshotEvent(ServentInfo current, BitcakeManager bitcakeManager, Map<Integer, ArrayList<Integer>> idBorderSet) {
				
		//System.out.println("PRE INKREMENTA MAPA CVORA " + AppConfig.myServentInfo.getId() + " IZGLEDA OVAKO: " +
		//AppConfig.myServentInfo.getSnapshotInfo());
		
		AppConfig.myServentInfo.incShanpshotNumber(AppConfig.myServentInfo.getSnapshotInfo(),
		AppConfig.myServentInfo.getMaster());
		
		//System.out.println("NAKON INKREMENTA MAPA CVORA " + AppConfig.myServentInfo.getId() + " IZGLEDA OVAKO: " +
		//AppConfig.myServentInfo.getSnapshotInfo());
		
		
			if(current.isInitiator()) {
	
				recordedAmount = getCurrentBitcakeAmount();
				LYSnapshotResult snapshotResult = new LYSnapshotResult(
						AppConfig.myServentInfo.getId(), recordedAmount, LiGiveHistory, LiGetHistory);

				
				current.addLYSnapshotInfo(
						AppConfig.myServentInfo.getId(),
						snapshotResult);
			}
			else {
								
				recordedAmount = getCurrentBitcakeAmount();
				LYSnapshotResult snapshotResult = new LYSnapshotResult(
						AppConfig.myServentInfo.getId(), recordedAmount, LiGiveHistory, LiGetHistory);				
				
				Message tellMessage = new LYTellMessage(
						AppConfig.myServentInfo, AppConfig.myServentInfo.getParent(), snapshotResult, idBorderSet);
				
				System.out.println(idBorderSet + " " + AppConfig.myServentInfo + " " + AppConfig.myServentInfo.getParent());
				MessageUtil.sendMessage(tellMessage);
				
				System.out.println("SERVERNT " + current.getId() + " JE LIST I SALJE SVOJ SNAPSHOT: " + AppConfig.myServentInfo.getParent());
			}

			//AppConfig.isWhite.set(true); //kad saljemo parentu rezultat, setujemo sebe na belo
		}
	
	public void parentInfo(ServentInfo parent) {
		
		AppConfig.myServentInfo.setParent(parent);
		if(!AppConfig.myServentInfo.getNotChildren().contains(parent.getId()))
			AppConfig.myServentInfo.setNotChildren(parent.getId());
		
		for (Integer neighbor : AppConfig.myServentInfo.getNeighbors()) {
			TellParentInfoMessage tellParentMessage = new TellParentInfoMessage(AppConfig.myServentInfo,  AppConfig.getInfoById(neighbor));
			MessageUtil.sendMessage(tellParentMessage);
			try {
				/*
				 * This sleep is here to artificially produce some white node -> red node messages.
				 * Not actually recommended, as we are sleeping while we have colorLock.
				 */
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		
	}
	
	public void markerEvent(int collectorId, SnapshotCollector snapshotCollector, int initId) {
		synchronized (AppConfig.colorLock) {
			AppConfig.isWhite.set(false); 
			
			for (Integer neighbor : AppConfig.myServentInfo.getNeighbors()) {
				Message clMarker = new LYMarkerMessage(AppConfig.myServentInfo, AppConfig.getInfoById(neighbor), collectorId);
				LYMarkerMessage marker = (LYMarkerMessage)clMarker;
				marker.setIniciatorId(initId);
				
				MessageUtil.sendMessage(clMarker);				
				try {
					/*
					 * This sleep is here to artificially produce some white node -> red node messages.
					 * Not actually recommended, as we are sleeping while we have colorLock.
					 */
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			
		}
	}
	
	private class MapValueUpdater implements BiFunction<Integer, Integer, Integer> {
		
		private int valueToAdd;
		
		public MapValueUpdater(int valueToAdd) {
			this.valueToAdd = valueToAdd;
		}
		
		@Override
		public Integer apply(Integer key, Integer oldValue) {
			return oldValue + valueToAdd;
		}
	}
	
	public void recordGiveTransaction(int neighbor, int amount) {
		giveHistory.compute(neighbor, new MapValueUpdater(amount));
	}
	
	public void recordGetTransaction(int neighbor, int amount) {
		getHistory.compute(neighbor, new MapValueUpdater(amount));
	}
	
	public void recordLiGetTransaction(TransactionMessage transaction) { 
			LiGetHistory.add(transaction);
	}
	
	public void recordLiGiveTransaction(TransactionMessage transaction) { 
			LiGiveHistory.add(transaction);
	}
	
}
