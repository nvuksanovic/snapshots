package app.snapshot_bitcake;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import servent.handler.TransactionHandler;
import servent.message.TransactionMessage;

/**
 * Snapshot result for servent with id serventId.
 * The amount of bitcakes on that servent is written in recordedAmount.
 * The channel messages are recorded in giveHistory and getHistory.
 * In Lai-Yang, the initiator has to reconcile the differences between
 * individual nodes, so we just let him know what we got and what we gave
 * and let him do the rest.
 * 
 * @author bmilojkovic
 *
 */
public class LYSnapshotResult implements Serializable {

	private static final long serialVersionUID = 8939516333227254439L;
	
	private final int serventId;
	private final int recordedAmount;
	private final ArrayList<TransactionMessage> giveHistory;
	private final ArrayList<TransactionMessage> getHistory;

	/*private final Map<Integer, Integer> giveHistory;
	private final Map<Integer, Integer> getHistory;
	
	public LYSnapshotResult(int serventId, int recordedAmount,
			Map<Integer, Integer> giveHistory, Map<Integer, Integer> getHistory) {
		this.serventId = serventId;
		this.recordedAmount = recordedAmount;
		this.giveHistory = new ConcurrentHashMap<>(giveHistory);
		this.getHistory = new ConcurrentHashMap<>(getHistory);
	}*/
	
	public LYSnapshotResult(int serventId, int recordedAmount,
			ArrayList<TransactionMessage> giveHistory, ArrayList<TransactionMessage> getHistory) {
		this.serventId = serventId;
		this.recordedAmount = recordedAmount;
		this.giveHistory = new ArrayList<>(giveHistory);
		this.getHistory = new ArrayList<>(getHistory);
	}
	
	public int getServentId() {
		return serventId;
	}
	public int getRecordedAmount() {
		return recordedAmount;
	}
	/*
	public Map<Integer, Integer> getGiveHistory() {
		return giveHistory;
	}
	public Map<Integer, Integer> getGetHistory() {
		return getHistory;
	}*/
	
	public ArrayList<TransactionMessage> getGiveHistory() {
		return giveHistory;
	}
	
	public ArrayList<TransactionMessage> getGetHistory() {
		return getHistory;
	}
}
