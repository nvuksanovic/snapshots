package app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.crypto.dsig.keyinfo.RetrievalMethod;

import app.snapshot_bitcake.LYSnapshotResult;

/**
 * This is an immutable class that holds all the information for a servent.
 *
 * @author bmilojkovic
 */
public class ServentInfo implements Serializable {

	private static final long serialVersionUID = 5304170042791281555L;
	private final int id;
	private final String ipAddress;
	private final int listenerPort;
	private final List<Integer> neighbors;
	private int master;
	private ServentInfo parent;
	private boolean isCentral;
	private boolean isLeaf;
	private boolean isInitiator;
	private boolean isFinished;
	private ConcurrentHashMap<Integer, LYSnapshotResult> result; 
	private ArrayList<Integer> children;
	private ArrayList<Integer> notChildren;
	private ArrayList<Integer> giveResult;
	private ArrayList<Integer> blanks;

	private Map<Integer, Integer> snapshotInfo = new ConcurrentHashMap<Integer, Integer>();
	private Map<Integer, ArrayList<Integer>> idBorderSet = new ConcurrentHashMap<Integer, ArrayList<Integer>>();
	
	public ServentInfo(String ipAddress, int id, int listenerPort, List<Integer> neighbors) {
		this.ipAddress = ipAddress;
		this.listenerPort = listenerPort;
		this.id = id;
		this.neighbors = neighbors;
		
		children = new ArrayList<Integer>();
		notChildren = new ArrayList<Integer>();
		giveResult = new ArrayList<Integer>();
		result = new ConcurrentHashMap<Integer, LYSnapshotResult>();
		blanks = new ArrayList<Integer>();
		
		master = -1;
		parent = null;
		isCentral = false;
		isInitiator = false;
		isFinished = false;
		isLeaf = false;
		
	
	}
	
	
	public void incShanpshotNumber(Map<Integer, Integer> snapshotInfo, int initId) {
		int valueToInc = snapshotInfo.get(initId);
		valueToInc++;
		snapshotInfo.replace(initId, valueToInc);
	}
	
	public ArrayList<Integer> getBlanks() {
		return blanks;
	}
	
	public ArrayList<Integer> getGiveResult() {
		return giveResult;
	}
	
	public void setGiveResult(ArrayList<Integer> giveResult) {
		this.giveResult = giveResult;
	}
	
	public boolean isLeaf() {
		return isLeaf;
	}
	
	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	
	public void setNotChildren(Integer notChild) {
		notChildren.add(notChild);
	}
	
	public ArrayList<Integer> getNotChildren() {
		return notChildren;
	}
	
	public Map<Integer, ArrayList<Integer>> getIdBorderSet() {
		return idBorderSet;
	}
	
	public void setIdBorderSet(Map<Integer, ArrayList<Integer>> idBorderSet) {
		this.idBorderSet = idBorderSet;
	}
	
	public Map<Integer, Integer> getSnapshotInfo() {
		return snapshotInfo;
	}
	
	public void setSnapshotInfo(Map<Integer, Integer> snapshotInfo) {
		this.snapshotInfo = snapshotInfo;
	}
	
	public boolean isFinished() {
		return isFinished;
	}
	
	public void finished(boolean isFinished) {
		this.isFinished = isFinished;
	}
	
	public boolean isInitiator() {
		return isInitiator;
	}
	
	public void setInitiator(boolean isInitiator) {
		this.isInitiator = isInitiator;
	}
	
	public ArrayList<Integer> getChildren() {
		return children;
	}
	
	public void setChildren(ArrayList<Integer> children) {
		this.children = children;
	}

	public boolean getCentral() {
		return isCentral;
	}
	
	public void setCentral(boolean isCentral) {
		this.isCentral = isCentral;
	}

	public int getMaster() {
		return master;
	}
	
	public void setMaster(int master) {
		this.master = master;
	}
	
	public ServentInfo getParent() {
		return parent;
	}
	
	public void setParent(ServentInfo parent) {
		this.parent = parent;
	}
	
	public int getParentId(ServentInfo parent) {
		return parent.getId();
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public int getListenerPort() {
		return listenerPort;
	}

	public int getId() {
		return id;
	}
	
	public List<Integer> getNeighbors() {
		return neighbors;
	}
	
	@Override
	public String toString() {
		return "[" + id + "|" + ipAddress + "|" + listenerPort + "]";
	}

	public void addLYSnapshotInfo(int id, LYSnapshotResult lySnapshotResult) {
		result.put(id, lySnapshotResult);
	}
	
	public ConcurrentHashMap<Integer, LYSnapshotResult> getSnapshotResult() {
		return result;
	}
}
