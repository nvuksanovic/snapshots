package servent.message;

public enum MessageType {
	TRANSACTION, LY_MARKER, LY_TELL, TELL_PARENT, TELL_BORDER, BLANK
}
