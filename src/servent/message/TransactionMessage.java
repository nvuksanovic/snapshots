package servent.message;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import app.AppConfig;
import app.ServentInfo;
import app.snapshot_bitcake.BitcakeManager;
import app.snapshot_bitcake.LYSnapshotResult;
import app.snapshot_bitcake.LaiYangBitcakeManager;
import servent.message.snapshot.LYMarkerMessage;

/**
 * Represents a bitcake transaction. We are sending some bitcakes to another node.
 * 
 * @author bmilojkovic
 *
 */
public class TransactionMessage extends BasicMessage {

	private static final long serialVersionUID = -333251402058492901L;

	private transient BitcakeManager bitcakeManager;
	private Map<Integer, Integer> snapshotInfo;
		
	public TransactionMessage(ServentInfo sender, ServentInfo receiver, int amount, BitcakeManager bitcakeManager) {
		super(MessageType.TRANSACTION, sender, receiver, String.valueOf(amount));
		this.bitcakeManager = bitcakeManager;
		snapshotInfo = new ConcurrentHashMap<Integer, Integer>();
	}
	
	public void setSnapshotInfo(Map<Integer, Integer> snapshotInfo) {
		this.snapshotInfo = snapshotInfo;
	}
	
	public Map<Integer, Integer> getSnapshotInfo() {
		return snapshotInfo;
	}
	
	
	private TransactionMessage(MessageType messageType, ServentInfo sender, ServentInfo receiver, boolean white, 
			List<ServentInfo> routeList, String messageText, int messageId, BitcakeManager bitcakeManager, Map<Integer, Integer> snapshotInfo) {
		super(messageType, sender, receiver, white, routeList, messageText, messageId);
		this.bitcakeManager = bitcakeManager;
		this.snapshotInfo = snapshotInfo;
	}
	
	@Override
	public Message setRedColor() {
		return new TransactionMessage(getMessageType(), getOriginalSenderInfo(), getReceiverInfo(), false, getRoute(),
				getMessageText(), getMessageId(), bitcakeManager, snapshotInfo);
	}
	/**
	 * We want to take away our amount exactly as we are sending, so our snapshots don't mess up.
	 * This method is invoked by the sender just before sending, and with a lock that guarantees
	 * that we are white when we are doing this in Chandy-Lamport.
	 */
	@Override
	public void sendEffect() {
		
		int amount = Integer.parseInt(getMessageText());
		
		bitcakeManager.takeSomeBitcakes(amount);
		
		if (bitcakeManager instanceof LaiYangBitcakeManager) { 
			LaiYangBitcakeManager lyFinancialManager = (LaiYangBitcakeManager)bitcakeManager;
			lyFinancialManager.recordLiGiveTransaction(this);
		}
	}
	
}
