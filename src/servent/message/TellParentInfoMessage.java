package servent.message;

import java.util.List;

import app.ServentInfo;
import app.snapshot_bitcake.LYSnapshotResult;
import servent.handler.MessageHandler;
import servent.message.snapshot.LYTellMessage;

public class TellParentInfoMessage extends BasicMessage{

	private static final long serialVersionUID = 1L;

	public TellParentInfoMessage(ServentInfo sender, ServentInfo receiver) {
		super(MessageType.TELL_PARENT, sender, receiver);
	}

	
	private TellParentInfoMessage(MessageType messageType, ServentInfo sender, ServentInfo receiver, 
			boolean white, List<ServentInfo> routeList, String messageText, int messageId) {
		super(messageType, sender, receiver, white, routeList, messageText, messageId);
	}
	

	@Override
	public Message setRedColor() {
		Message toReturn = new TellParentInfoMessage(getMessageType(), getOriginalSenderInfo(), getReceiverInfo(),
				false, getRoute(), getMessageText(), getMessageId());
		return toReturn;
	}
}
