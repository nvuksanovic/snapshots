package servent.message.snapshot;

import java.util.List;
import app.ServentInfo;
import servent.message.BasicMessage;
import servent.message.Message;
import servent.message.MessageType;

public class LYMarkerMessage extends BasicMessage {

	private static final long serialVersionUID = 388942509576636228L;
	private int iniciatorId; 
	
	public LYMarkerMessage(ServentInfo sender, ServentInfo receiver, int collectorId) {
		super(MessageType.LY_MARKER, sender, receiver, String.valueOf(collectorId));
		
		this.iniciatorId = collectorId;
	}
	
	private LYMarkerMessage(MessageType messageType, ServentInfo sender, ServentInfo receiver, 
			boolean white, List<ServentInfo> routeList, String messageText, int messageId) {
		super(messageType, sender, receiver, white, routeList, messageText, messageId);
	
	}

	
	public int getIniciatorId() {
		return iniciatorId;
	}
	public void setIniciatorId(int iniciatorId) {
		this.iniciatorId = iniciatorId;
	}
	
	@Override
	public Message setRedColor() {
		return new LYMarkerMessage(getMessageType(), getOriginalSenderInfo(), getReceiverInfo(), false, getRoute(),
				getMessageText(), getMessageId());
	}
}
