package servent.message.snapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.ServentInfo;
import app.snapshot_bitcake.LYSnapshotResult;
import servent.message.BasicMessage;
import servent.message.Message;
import servent.message.MessageType;

public class LYTellMessage extends BasicMessage {

	private static final long serialVersionUID = 3116394054726162318L;

	private LYSnapshotResult lySnapshotResult;
	private Map<Integer, ArrayList<Integer>> idBorderSet;
	
	public LYTellMessage(ServentInfo sender, ServentInfo receiver, LYSnapshotResult lySnapshotResult, 
			Map<Integer, ArrayList<Integer>> idBorderSet) {
		
		super(MessageType.LY_TELL, sender, receiver);
		
		this.lySnapshotResult = lySnapshotResult;
		this.idBorderSet = idBorderSet;
	}
	
	private LYTellMessage(MessageType messageType, ServentInfo sender, ServentInfo receiver, 
			boolean white, List<ServentInfo> routeList, String messageText, int messageId,
			LYSnapshotResult lySnapshotResult, Map<Integer, ArrayList<Integer>> idBorderSet) {
		super(messageType, sender, receiver, white, routeList, messageText, messageId);
		
		this.lySnapshotResult = lySnapshotResult;
		this.idBorderSet = idBorderSet;

	}

	public LYSnapshotResult getLYSnapshotResult() {
		return lySnapshotResult;
	}
	
	public Map<Integer, ArrayList<Integer>> getIdBorderSet() {
		return idBorderSet;
	}
	
	@Override
	public Message setRedColor() {
		Message toReturn = new LYTellMessage(getMessageType(), getOriginalSenderInfo(), getReceiverInfo(),
				false, getRoute(), getMessageText(), getMessageId(), getLYSnapshotResult(), getIdBorderSet());
		return toReturn;
	}
}
