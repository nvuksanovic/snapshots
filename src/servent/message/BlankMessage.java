package servent.message;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.ServentInfo;

public class BlankMessage extends BasicMessage{
	
	private boolean isBlank = false;
	private Map<Integer, ArrayList<Integer>> idBorderSet;

	public BlankMessage(MessageType type, ServentInfo originalSenderInfo, ServentInfo receiverInfo) {
		super(MessageType.BLANK, originalSenderInfo, receiverInfo);
		
	}
	
	private BlankMessage(MessageType messageType, ServentInfo sender, ServentInfo receiver, 
			boolean white, List<ServentInfo> routeList, String messageText, int messageId) {
		super(messageType, sender, receiver, white, routeList, messageText, messageId);
	}
	

	@Override
	public Message setRedColor() {
		Message toReturn = new BlankMessage(getMessageType(), getOriginalSenderInfo(), getReceiverInfo(),
				false, getRoute(), getMessageText(), getMessageId());
		return toReturn;
	}

	public void setIdBorderSet(Map<Integer, ArrayList<Integer>> idBorderSet) {
		this.idBorderSet = idBorderSet;
	}
	
	public Map<Integer, ArrayList<Integer>> getIdBorderSet() {
		return idBorderSet;
	}
	
	public boolean isBlank() {
		return isBlank;
	}
	
	public void setBlank(boolean isBlank) {
		this.isBlank = isBlank;
	}
	
}
