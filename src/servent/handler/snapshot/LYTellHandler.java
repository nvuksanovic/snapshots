package servent.handler.snapshot;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import org.omg.PortableInterceptor.INACTIVE;

import app.AppConfig;
import app.ServentInfo;
import app.snapshot_bitcake.BitcakeManager;
import app.snapshot_bitcake.LYSnapshotResult;
import app.snapshot_bitcake.LaiYangBitcakeManager;
import app.snapshot_bitcake.SnapshotCollector;
import servent.SimpleServentListener;
import servent.handler.MessageHandler;
import servent.handler.TellParentInfoHandler;
import servent.message.Message;
import servent.message.MessageType;
import servent.message.snapshot.LYTellMessage;

public class LYTellHandler implements MessageHandler {

	private Message clientMessage;
	private SnapshotCollector snapshotCollector;
	private ServentInfo current;
	private BitcakeManager bitcakeManager;
	private Map<Integer, ArrayList<Integer>> idBorderSet;
	private AtomicBoolean isDone = new AtomicBoolean(false);

	public LYTellHandler(Message clientMessage, SnapshotCollector snapshotCollector) {
		this.clientMessage = clientMessage;
		this.snapshotCollector = snapshotCollector;
	}

	public LYTellHandler(Message clientMessage, ServentInfo current, BitcakeManager bitcakeManager,
			Map<Integer, ArrayList<Integer>> idBorderSet) {
		this.clientMessage = clientMessage;
		this.current = current;
		this.bitcakeManager = bitcakeManager;
		this.idBorderSet = idBorderSet;
	}

	@Override
	public void run() {

		LYTellMessage LYTellmessage = (LYTellMessage) clientMessage;
		
		if (clientMessage.getMessageType() == MessageType.LY_TELL) {
			
			current.getSnapshotResult().put(LYTellmessage.getOriginalSenderInfo().getId(),
				LYTellmessage.getLYSnapshotResult());
		}

		System.out.println("children " + current.getChildren());
		System.out.println("not children " + current.getNotChildren());

		if (current.getChildren().size() + current.getNotChildren().size() == current.getNeighbors().size())
			isDone.getAndSet(true);
		

		if (isDone.get() && !current.isFinished()) {
			
			System.out.println("Provera " + LYTellmessage.getIdBorderSet() + " od cvora "
					+ clientMessage.getOriginalSenderInfo().getId());
			// System.out.println("MOJ BORDER " + AppConfig.myServentInfo.getIdBorderSet());

			if (clientMessage.getMessageType() == MessageType.LY_TELL) {
				
				
				Map<Integer, ArrayList<Integer>> myIdBorderSet = AppConfig.myServentInfo.getIdBorderSet();

				for (Entry<Integer, ArrayList<Integer>> entry : myIdBorderSet.entrySet()) {
					if (entry.getKey() == LYTellmessage.getOriginalSenderInfo().getMaster()) {

						for (int newInitId : LYTellmessage.getIdBorderSet()
								.get(LYTellmessage.getOriginalSenderInfo().getMaster())) {
							if (!entry.getValue().contains(newInitId)) {
								myIdBorderSet.get(entry.getKey()).add(newInitId);
							}
						}

					}
				}

				System.out.println("MOJ BORDER SAD JE: " + myIdBorderSet);

				if (isDone(current.getSnapshotResult(), current.getChildren())) {

					current.finished(true);
					LaiYangBitcakeManager LYBitcakeManager = (LaiYangBitcakeManager) bitcakeManager;
					LYBitcakeManager.snapshotEvent(AppConfig.myServentInfo, bitcakeManager, idBorderSet);
				}

			} else {
				
				AppConfig.timestampedErrorPrint("Tell amount handler got: " + clientMessage);
			}
		}
		

	}

	private boolean isDone(ConcurrentHashMap<Integer, LYSnapshotResult> snapshotMap, ArrayList<Integer> childrenList) {

		boolean result = false;
		for (Integer childID : childrenList) {
			if (snapshotMap.containsKey(childID)) {
				result = true;
			} else {
				result = false;
				break;
			}
		}

		return result;

	}

}
