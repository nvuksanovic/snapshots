package servent.handler;

import app.AppConfig;
import app.ServentInfo;
import servent.message.Message;
import servent.message.TellParentInfoMessage;

public class TellParentInfoHandler implements MessageHandler {
	
	private Message clientMessage;

	public TellParentInfoHandler(Message clientMessage) {
		this.clientMessage = clientMessage;
	}

	@Override
	public void run() {
		
		try {
			for(Integer neighbor: AppConfig.myServentInfo.getNeighbors()) {
				if(neighbor == clientMessage.getOriginalSenderInfo().getId()) {
					AppConfig.getInfoById(neighbor).setParent(clientMessage.getOriginalSenderInfo().getParent());
					
				}

			}
			
			//if i'm parent - load my children list and set me as central
			
			if(AppConfig.myServentInfo.getId() == clientMessage.getOriginalSenderInfo().getParent().getId()) {
				AppConfig.myServentInfo.getChildren().add(clientMessage.getOriginalSenderInfo().getId());
				System.out.println("MY CHILDREN: " + AppConfig.myServentInfo.getChildren());
				AppConfig.myServentInfo.setLeaf(false);
				if(AppConfig.myServentInfo.getCentral() == false) {
					AppConfig.myServentInfo.setCentral(true); //ne znam da li za inicijatora treba ovo?
					System.out.println("centralni " + AppConfig.myServentInfo.getId());
				}
				System.out.println("CENTRALNI");
					
			}
			
			else {
				if(!AppConfig.myServentInfo.getNotChildren().contains(clientMessage.getOriginalSenderInfo().getId()))
					AppConfig.myServentInfo.setNotChildren(clientMessage.getOriginalSenderInfo().getId());
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			
		}
		

	}

}
