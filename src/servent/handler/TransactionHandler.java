package servent.handler;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

import app.AppConfig;
import app.snapshot_bitcake.BitcakeManager;
import app.snapshot_bitcake.LaiYangBitcakeManager;
import servent.message.Message;
import servent.message.MessageType;
import servent.message.TransactionMessage;

public class TransactionHandler implements MessageHandler {

	private Message clientMessage;
	private BitcakeManager bitcakeManager;
	private AtomicBoolean record = new AtomicBoolean(false);
	
	public TransactionHandler(Message clientMessage, BitcakeManager bitcakeManager) {
		this.clientMessage = clientMessage;
		this.bitcakeManager = bitcakeManager;
	}

	@Override
	public void run() {
		
		//belezim kod sebe u istoriju uredjeni par koji dobijem preko transakcije
		
		if (clientMessage.getMessageType() == MessageType.TRANSACTION) {
			TransactionMessage transaction = (TransactionMessage)clientMessage;
			String amountString = clientMessage.getMessageText();
			int amountNumber = 0;
			try {
				amountNumber = Integer.parseInt(amountString);
			} catch (NumberFormatException e) {
				AppConfig.timestampedErrorPrint("Couldn't parse amount: " + amountString);
				return;
			}
			
			
			//ovo je ona situacija kada smo dobili crvenu poruku, a zadovoljava uslov:
			//njegov (sender) snapshot number je veci od naseg snapshot number-a
			//to znaci da hoces i tu transkciju da ukljucimo u istoriju
			if(!clientMessage.isWhite()) {
				
				for(Entry<Integer, Integer> entry: clientMessage.getOriginalSenderInfo().getSnapshotInfo().entrySet()) {
					if(AppConfig.myServentInfo.getSnapshotInfo().containsKey(entry.getKey())) {
						if(AppConfig.myServentInfo.getSnapshotInfo().get(entry.getKey()) < entry.getValue()) { 
							record.getAndSet(true);
							break;
						}
							
					}
				}
				
			}
			
			transaction.setSnapshotInfo(clientMessage.getOriginalSenderInfo().getSnapshotInfo());


			bitcakeManager.addSomeBitcakes(amountNumber);

			synchronized (AppConfig.colorLock) {
				if ((bitcakeManager instanceof LaiYangBitcakeManager && clientMessage.isWhite()) || record.get()) { 

					LaiYangBitcakeManager lyFinancialManager = (LaiYangBitcakeManager)bitcakeManager;
					
					//lyFinancialManager.recordGetTransaction(clientMessage.getOriginalSenderInfo().getId(), amountNumber);
					lyFinancialManager.recordLiGetTransaction(transaction);
			
					record.getAndSet(false);
				}
			}
		} else {
			AppConfig.timestampedErrorPrint("Transaction handler got: " + clientMessage);
		}
	}

}
