package servent.handler;

import java.util.ArrayList;
import java.util.Map;

import app.AppConfig;
import servent.message.BlankMessage;
import servent.message.Message;

public class BlankHandler implements MessageHandler{
	
	private Message clientMessage;
	private boolean waiting = false;
	private ArrayList<Integer> neighbors;
	
	public BlankHandler(Message clientMessage) {
		this.clientMessage = clientMessage;
		neighbors = new ArrayList<Integer>();

	}

	@Override
	public void run() {
		
		if(clientMessage instanceof BlankMessage) {
			
			BlankMessage blankMessage = (BlankMessage)clientMessage;
			
			Map<Integer, ArrayList<Integer>> idBorderSet = blankMessage.getIdBorderSet();
		
			neighbors = AppConfig.myServentInfo.getIdBorderSet().get(AppConfig.myServentInfo.getId());
			
			while(waiting) {
				
				if(blankMessage.isBlank()) {
					AppConfig.myServentInfo.getBlanks().add(clientMessage.getOriginalSenderInfo().getId());
				}
				
				if(AppConfig.myServentInfo.getBlanks().size() == neighbors.size())
					waiting = false;
				
			}
			
			
		}
	}

}
