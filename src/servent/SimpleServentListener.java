package servent;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.border.Border;

import app.AppConfig;
import app.Cancellable;
import app.ServentInfo;
import app.snapshot_bitcake.LaiYangBitcakeManager;
import app.snapshot_bitcake.SnapshotCollector;
import servent.handler.BlankHandler;
import servent.handler.MessageHandler;
import servent.handler.NullHandler;
import servent.handler.TellParentInfoHandler;
import servent.handler.TransactionHandler;
import servent.handler.snapshot.LYMarkerHandler;
import servent.handler.snapshot.LYTellHandler;
import servent.message.BasicMessage;
import servent.message.Message;
import servent.message.MessageType;
import servent.message.snapshot.LYMarkerMessage;
import servent.message.snapshot.LYTellMessage;
import servent.message.util.MessageUtil;

public class SimpleServentListener implements Runnable, Cancellable {

	private volatile boolean working = true;
	
	private SnapshotCollector snapshotCollector;
	private Map<Integer, ArrayList<Integer>> idBorderSet = new ConcurrentHashMap<Integer, ArrayList<Integer>>();
	private AtomicBoolean isLeaf = new AtomicBoolean(false);
	private AtomicBoolean startCollecting = new AtomicBoolean(false);
	private ArrayList<Integer> initNeighborId;
	
	public SimpleServentListener(SnapshotCollector snapshotCollector) {
		this.snapshotCollector = snapshotCollector;
		
		for(int i = 0; i < AppConfig.getInitiatorsList().size(); i++) {
		
			initNeighborId = new ArrayList<Integer>();
			initNeighborId.add(AppConfig.getInitiatorsList().get(i));
			idBorderSet.put(AppConfig.getInitiatorsList().get(i), initNeighborId);
		}
		
		AppConfig.myServentInfo.setIdBorderSet(idBorderSet);
		
	}

	/*
	 * Thread pool for executing the handlers. Each client will get it's own handler thread.
	 */
	private final ExecutorService threadPool = Executors.newWorkStealingPool();
	
	private List<Message> redMessages = new ArrayList<>();
	
	@Override
	public void run() {
		ServerSocket listenerSocket = null;
		try {
			listenerSocket = new ServerSocket(AppConfig.myServentInfo.getListenerPort(), 100);
			/*
			 * If there is no connection after 1s, wake up and see if we should terminate.
			 */
			listenerSocket.setSoTimeout(1000);
		} catch (IOException e) {
			AppConfig.timestampedErrorPrint("Couldn't open listener socket on: " + AppConfig.myServentInfo.getListenerPort());
			System.exit(0);
		}
		
		
		while (working) {
			try {
				
				Message clientMessage;

				/*
				 * Lai-Yang stuff. Process any red messages we got before we got the marker.
				 * The marker contains the collector id, so we need to process that as our first
				 * red message. 
				 */
				if (AppConfig.isWhite.get() == false && redMessages.size() > 0) {
					clientMessage = redMessages.remove(0);
				} else {
					/*
					 * This blocks for up to 1s, after which SocketTimeoutException is thrown.
					 */
					Socket clientSocket = listenerSocket.accept();
					
					//GOT A MESSAGE! <3
					clientMessage = MessageUtil.readMessage(clientSocket);
					
					
					if(clientMessage instanceof LYMarkerMessage) {
						

						if(AppConfig.myServentInfo.getMaster() == -1 && Integer.parseInt(clientMessage.getMessageText()) != AppConfig.myServentInfo.getId()) {
							AppConfig.myServentInfo.setMaster(clientMessage.getOriginalSenderInfo().getMaster());
						}
						
						
						if(AppConfig.myServentInfo.getParent() == null && !AppConfig.myServentInfo.isInitiator()) {
							LaiYangBitcakeManager lyFinancialManager =
									(LaiYangBitcakeManager)snapshotCollector.getBitcakeManager();
							lyFinancialManager.parentInfo(clientMessage.getOriginalSenderInfo());
							
						}
						
							
						System.out.println("SERVENT: " + AppConfig.myServentInfo.getId() + " HAS PARENT " + AppConfig.myServentInfo.getParent());


						
						if(AppConfig.myServentInfo.getMaster() == clientMessage.getOriginalSenderInfo().getMaster()) { //same region
							
							
							if(!startCollecting.get() && !AppConfig.myServentInfo.isInitiator() /*AppConfig.myServentInfo.getMaster() != AppConfig.myServentInfo.getId()*/) {
								
								
								for(Integer neighbor: AppConfig.myServentInfo.getNeighbors()) { //takodje proveriti da trenutni cvor nije parent nijednom svom susedu
									
									
									
									if(AppConfig.myServentInfo.getNeighbors().contains(AppConfig.myServentInfo.getMaster())) {
										
									
										if(AppConfig.getInfoById(neighbor).getParent() == null && neighbor != AppConfig.myServentInfo.getMaster()) {
											isLeaf.getAndSet(false); 
											AppConfig.myServentInfo.setLeaf(false);
											break;
										}
										else {
											isLeaf.getAndSet(true); 
											AppConfig.myServentInfo.setLeaf(true);
										}
									}
									else {
										
										if(AppConfig.getInfoById(neighbor).getParent() == null) {
											isLeaf.getAndSet(false); 
											AppConfig.myServentInfo.setLeaf(false);
											break;
										}
										else {
											isLeaf.getAndSet(true); 
											AppConfig.myServentInfo.setLeaf(true);
										}
									}
									
									System.out.println("NEIGHBOR " + neighbor + " PARENT " + AppConfig.getInfoById(neighbor).getParent());

								}
								
								System.out.println("SERVENT " + AppConfig.myServentInfo.getId() + " IS LEAF: " +  isLeaf);
								
								System.out.println("ISTI REGION!");
							}
							
						}
						
						else if(!startCollecting.get()) {
							//different region

							System.out.println("RAZLICIT REGION!");
							isLeaf.getAndSet(true);
							AppConfig.myServentInfo.setLeaf(true);
							
							idBorderSet.get(AppConfig.myServentInfo.getMaster()).add(clientMessage.getOriginalSenderInfo().getMaster());
							
							System.out.println("BORDERSET KOD CVORA " + AppConfig.myServentInfo.getId() + " : " + idBorderSet);
							
							AppConfig.myServentInfo.setIdBorderSet(idBorderSet);

						}
						
						if(AppConfig.myServentInfo.isLeaf() && !AppConfig.myServentInfo.isInitiator()) {
							LaiYangBitcakeManager lyFinancialManager =
									(LaiYangBitcakeManager)snapshotCollector.getBitcakeManager();
							lyFinancialManager.snapshotEvent(AppConfig.myServentInfo, snapshotCollector.getBitcakeManager(), idBorderSet);
							
							System.out.println("LIST: " + AppConfig.myServentInfo.getId() + " ZAPOCINJE SNAPSHOT");
							isLeaf.getAndSet(false);
							startCollecting.getAndSet(true);
						}
						
					}
				}
				synchronized (AppConfig.colorLock) {
					if (clientMessage.isWhite() == false && AppConfig.isWhite.get()) {
						/*
						 * If the message is red, we are white, and the message isn't a marker,
						 * then store it. We will get the marker soon, and then we will process
						 * this message. The point is, we need the marker to know who to send
						 * our info to, so this is the simplest way to work around that.
						 */
						if (clientMessage.getMessageType() != MessageType.LY_MARKER) {
							redMessages.add(clientMessage);
							continue;
						} else {
							
							if(clientMessage instanceof LYMarkerMessage) {
								LYMarkerMessage marker = (LYMarkerMessage)clientMessage;
								
								LaiYangBitcakeManager lyFinancialManager =
										(LaiYangBitcakeManager)snapshotCollector.getBitcakeManager();
								lyFinancialManager.markerEvent(
										Integer.parseInt(clientMessage.getMessageText()), snapshotCollector, marker.getIniciatorId());
							}
							
						}
					}
					
				}
				
				
				MessageHandler messageHandler = new NullHandler(clientMessage);

				/*
				 * Each message type has it's own handler.
				 * If we can get away with stateless handlers, we will,
				 * because that way is much simpler and less error prone.
				 */
				switch (clientMessage.getMessageType()) {
				case TRANSACTION:
					messageHandler = new TransactionHandler(clientMessage, snapshotCollector.getBitcakeManager());
					break;
				case LY_MARKER:
					messageHandler = new LYMarkerHandler(clientMessage, snapshotCollector.getBitcakeManager());
					break;
				case LY_TELL:
					messageHandler = new LYTellHandler(clientMessage, AppConfig.myServentInfo, snapshotCollector.getBitcakeManager(), idBorderSet);
					break;
				case TELL_PARENT:
					messageHandler = new TellParentInfoHandler(clientMessage);
					break;
				case BLANK:
					messageHandler = new BlankHandler(clientMessage);
					
				}
				
				threadPool.submit(messageHandler);
			} catch (SocketTimeoutException timeoutEx) {
				//Uncomment the next line to see that we are waking up every second.
//				AppConfig.timedStandardPrint("Waiting...");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void stop() {
		this.working = false;
	}

}
